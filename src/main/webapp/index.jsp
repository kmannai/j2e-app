<html>
<body>
<h2>J2E Session & Filter example!</h2>
<h4>To start adding information, you can start by this form : </h4>
<a href="/generalInfoForm" rel="noreferrer" target="_blank">GeneralInfoForm</a>
<h4>then :</h4>
<a href="/personalInfoForm" rel="noreferrer" target="_blank">PersonalInfoForm</a>
<h4>To display saved session values :</h4>
<a href="/myInfo" rel="noreferrer" target="_blank">My information</a>
<h4>To logout :</h4>
<a href="/deleteInfo" rel="noreferrer" target="_blank">Logout</a>
</body>
</html>
